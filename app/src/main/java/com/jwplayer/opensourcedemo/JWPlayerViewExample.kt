package com.jwplayer.opensourcedemo

import android.Manifest
import android.content.res.Configuration
import android.net.Uri
import android.os.Bundle
import android.text.TextUtils
import android.view.KeyEvent
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import com.downloader.Error
import com.downloader.OnDownloadListener
import com.downloader.PRDownloader
import com.intentfilter.androidpermissions.PermissionManager
import com.longtailvideo.jwplayer.JWPlayerView
import com.longtailvideo.jwplayer.core.PlayerState
import com.longtailvideo.jwplayer.events.FullscreenEvent
import com.longtailvideo.jwplayer.events.listeners.VideoPlayerEvents
import com.longtailvideo.jwplayer.media.playlists.PlaylistItem
import java.io.File

/**
 * JWPlayerViewExample class is responsible to download JW CDN video, save it and play it in offline mode
 */
class JWPlayerViewExample : AppCompatActivity(), VideoPlayerEvents.OnFullscreenListener {

    companion object {
        private const val DEMO_VIDEO_MEDIA_ID = "C4zq2wQd"
        private const val DEMO_VIDEO_SIG = "6269d10427ed4194d35bbbd73035b7c2"
        private const val DEMO_VIDEO_EXPIRY = "1580468043"
    }
    private lateinit var progressTextView: TextView
    private lateinit var progressBar: ProgressBar
    private var downloadId: Int = 0
    private lateinit var mPlayerView: JWPlayerView
    private lateinit var urlEditText: EditText
    private lateinit var sigEditText: EditText
    private lateinit var expiryEditText: EditText
    private lateinit var downloadButton: Button
    private lateinit var dirPath: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_jwplayerview)

        findViews()
        setListeners()
        askPermission()
        setupDemoUrl()
    }

    private fun setupDemoUrl() {
        urlEditText.setText(DEMO_VIDEO_MEDIA_ID)
        sigEditText.setText(DEMO_VIDEO_SIG)
        expiryEditText.setText(DEMO_VIDEO_EXPIRY)
    }

    private fun findViews() {
        mPlayerView = findViewById(R.id.jwPlayer)
        urlEditText = findViewById(R.id.urlView)
        sigEditText = findViewById(R.id.sigView)
        expiryEditText = findViewById(R.id.expiryView)
        downloadButton = findViewById(R.id.downloadButton)
        progressTextView = findViewById(R.id.progressTextView)
        progressBar = findViewById(R.id.progressBar)
    }

    private fun setListeners() {
        mPlayerView.addOnFullscreenListener(this)
        KeepScreenOnHandler(mPlayerView, window)

        downloadButton.setOnClickListener { _ ->
            val mediaID = urlEditText.text.toString()

            if (TextUtils.isEmpty(mediaID)) {
                Toast.makeText(this@JWPlayerViewExample, getString(R.string.media_id_missing), Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            val url = Utils.getVideoUrl(mediaID, sigEditText.text.toString(), expiryEditText.text.toString())
            progressBar.visibility = View.VISIBLE
            progressTextView.visibility = View.VISIBLE
            mPlayerView.visibility = View.GONE
            if (mPlayerView.state == PlayerState.PLAYING) {
                mPlayerView.stop()
            }

            val file_name = Utils.createVideoName(mediaID)
            downloadButton.isEnabled = false
            progressBar.isIndeterminate = true

            downloadId = PRDownloader.download(url, dirPath, file_name)
                    .build()
                    .setOnProgressListener { progress ->
                        val progressPercent = progress.currentBytes * 100 / progress.totalBytes
                        progressBar.progress = progressPercent.toInt()
                        progressTextView.text = Utils.getProgressDisplayLine(progress.currentBytes, progress.totalBytes)
                        progressBar.isIndeterminate = false
                    }
                    .start(object : OnDownloadListener {
                        override fun onDownloadComplete() {
                            runOnUiThread {
                                val file = File(dirPath, file_name)
                                Utils.saveVideoPath(file.absolutePath, applicationContext)
                                val playListItem = PlaylistItem.Builder()
                                        .file(Uri.fromFile(file).toString())
                                        .build()
                                progressBar.visibility = View.GONE
                                progressTextView.visibility = View.GONE
                                mPlayerView.visibility = View.VISIBLE
                                mPlayerView.load(playListItem)
                                mPlayerView.play()
                                downloadButton.isEnabled = true
                            }
                        }

                        override fun onError(error: Error) {
                            if (error.isConnectionError) {
                                Toast.makeText(applicationContext, getString(R.string.network_not_available), Toast.LENGTH_SHORT).show()
                            } else if (error.isServerError) {
                                Toast.makeText(applicationContext, error.serverErrorMessage, Toast.LENGTH_SHORT).show()
                            } else {
                                Toast.makeText(applicationContext, getString(R.string.some_error_occurred), Toast.LENGTH_SHORT).show()
                            }
                            progressTextView.text = ""
                            progressBar.progress = 0
                            downloadId = 0
                            progressBar.isIndeterminate = false
                            progressBar.visibility = View.GONE
                            progressTextView.visibility = View.GONE
                            downloadButton.isEnabled = true


                        }
                    })
        }
    }

    private fun askPermission() {
        val permissionManager = PermissionManager.getInstance(this)
        permissionManager.checkPermissions(listOf(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE),
                object : PermissionManager.PermissionRequestListener {
                    override fun onPermissionGranted() {
                        downloadButton.isEnabled = true
                        setUpJWPlayer()
                    }

                    override fun onPermissionDenied() {
                        Toast.makeText(
                                applicationContext,
                                getString(R.string.storage_permission),
                                Toast.LENGTH_LONG
                        ).show()
                    }
                })
    }

    private fun setUpJWPlayer() {
        dirPath = Utils.getRootDirPath(applicationContext)

        if (Utils.isVideoDownloaded(applicationContext)) {
            progressBar.visibility = View.GONE
            progressTextView.visibility = View.GONE
            mPlayerView.visibility = View.VISIBLE
            val file = File(dirPath, Utils.getVideoName(applicationContext))
            val playListItem = PlaylistItem.Builder().file(Uri.fromFile(file).toString()).build()
            mPlayerView.load(playListItem)
            mPlayerView.play()
        }
    }

    override fun onResume() {
        super.onResume()
        mPlayerView.onResume()
    }

    override fun onPause() {
        super.onPause()
        mPlayerView.onPause()
    }

    override fun onStop() {
        super.onStop()
        mPlayerView.stop()
    }

    override fun onDestroy() {
        super.onDestroy()
        mPlayerView.onDestroy()
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        // Set fullscreen when the device is rotated to landscape
        mPlayerView.setFullscreen(newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE, true)
        super.onConfigurationChanged(newConfig)
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        // Exit fullscreen when the user pressed the Back downloadButton
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (mPlayerView.fullscreen) {
                mPlayerView.setFullscreen(false, true)
                return false
            }
        }
        return super.onKeyDown(keyCode, event)
    }

    override fun onFullscreen(fullscreenEvent: FullscreenEvent) {
        val actionBar = supportActionBar
        if (actionBar != null) {
            if (fullscreenEvent.fullscreen) {
                actionBar.hide()
            } else {
                actionBar.show()
            }
        }
    }
}
