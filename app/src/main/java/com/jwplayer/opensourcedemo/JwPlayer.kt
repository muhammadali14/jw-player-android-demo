package com.jwplayer.opensourcedemo

import android.app.Application
import com.downloader.PRDownloader
import com.downloader.PRDownloaderConfig

class JwPlayer : Application() {

    companion object {
        private const val READ_TIME_OUT = 300000
        private const val CONNECTION_TIME_OUT = 300000
    }
    override fun onCreate() {
        super.onCreate()
        configurePWDownloader()
    }

    private fun configurePWDownloader() {
        val config = PRDownloaderConfig.newBuilder()
            .setDatabaseEnabled(true)
            .setReadTimeout(READ_TIME_OUT)
            .setConnectTimeout(CONNECTION_TIME_OUT)
            .build()
        PRDownloader.initialize(this, config)
    }
}
