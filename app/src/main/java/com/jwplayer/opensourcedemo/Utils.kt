package com.jwplayer.opensourcedemo

import android.content.Context
import android.os.Environment
import androidx.core.content.ContextCompat
import java.util.*

/**
 * A Utils class which helps on getting video path from external storage and calculate progress dialog progress
 */

object Utils {

    private const val PREFERENCE = "jwPlayer"
    private const val VIDEO_NAME = "jwVideo"
    private const val BASE_URL = "https://cdn.jwplayer.com/videos/m6NA0Skf-"
    private const val VIDEO_TYPE = ".mp4"
    private const val VIDEO_NAME_PREFIX = "jw"
    private const val PATH_SEPARATOR = "/"
    private const val SIG = "sig="
    private const val EXPIRY = "exp="
    private const val QUERY_PARAM_SEPARATOR = "&"

    fun getRootDirPath(applicationContext: Context): String {
        return if (Environment.MEDIA_MOUNTED == Environment.getExternalStorageState()) {
            ContextCompat.getExternalFilesDirs(applicationContext, null).first().absolutePath
        } else {
            applicationContext.filesDir.absolutePath
        }
    }

    fun getProgressDisplayLine(currentBytes: Long, totalBytes: Long): String {
        return "${getBytesToMBString(currentBytes)}$PATH_SEPARATOR${getBytesToMBString(totalBytes)}"
    }

    fun isVideoDownloaded(applicationContext: Context): Boolean {
        val prefs = applicationContext.getSharedPreferences(PREFERENCE, 0)
        val videoPathExist = prefs.getString(VIDEO_NAME, null)
        return videoPathExist != null
    }

    fun getVideoName(applicationContext: Context): String {
        val prefs = applicationContext.getSharedPreferences(PREFERENCE, 0)
        val path = prefs.getString(VIDEO_NAME, null)
        val filename = path?.substring(path.lastIndexOf(PATH_SEPARATOR) + 1)
        return filename!!
    }

    fun getVideoUrl(mediaId: String, sig: String, expiry: String): String {
        return "$BASE_URL$mediaId$VIDEO_TYPE?$SIG$sig$QUERY_PARAM_SEPARATOR$EXPIRY$expiry"
    }

    fun createVideoName(mediaId: String): String {
        return "$VIDEO_NAME_PREFIX$mediaId$VIDEO_TYPE"
    }

    fun saveVideoPath(path: String, applicationContext: Context) {
        val sharedPref = applicationContext.getSharedPreferences(PREFERENCE, 0)
        val editor = sharedPref.edit()
        editor.putString(VIDEO_NAME, path)
        editor.apply()
    }

    private fun getBytesToMBString(bytes: Long): String {
        return String.format(Locale.ENGLISH, "%.2fMb", bytes / (1024.00 * 1024.00))
    }
}
